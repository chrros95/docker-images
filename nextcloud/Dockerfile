FROM nextcloud:%%VERSION%%-apache

RUN --mount=target=/var/lib/apt/lists,type=cache,sharing=locked \
    --mount=target=/var/cache/apt,type=cache,sharing=locked \
    rm -f /etc/apt/apt.conf.d/docker-clean; \
    set -ex; \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        ffmpeg \
        ghostscript \
        libmagickcore-6.q16-6-extra \
        procps \
        smbclient \
    ;

RUN --mount=target=/var/lib/apt/lists,type=cache,sharing=locked \
    --mount=target=/var/cache/apt,type=cache,sharing=locked \
    rm -f /etc/apt/apt.conf.d/docker-clean; \
    set -ex; \
    \
    savedAptMark="$(apt-mark showmanual)"; \
    \
    apt-get update; \
    apt-get install -y --no-install-recommends \
        libbz2-dev \
        libc-client-dev \
        libkrb5-dev \
        libsmbclient-dev \
    ; \
    \
    docker-php-ext-configure imap --with-kerberos --with-imap-ssl; \
    docker-php-ext-install \
        bz2 \
        imap \
    ; \
    pecl install smbclient; \
    docker-php-ext-enable smbclient; \
    \
# reset apt-mark's "manual" list so that "purge --auto-remove" will remove all build dependencies
    apt-mark auto '.*' > /dev/null; \
    apt-mark manual $savedAptMark; \
    ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
        | awk '/=>/ { so = $(NF-1); if (index(so, "/usr/local/") == 1) { next }; gsub("^/(usr/)?", "", so); print so }' \
        | sort -u \
        | xargs -r dpkg-query --search \
        | cut -d: -f1 \
        | sort -u \
        | xargs -rt apt-mark manual; \
    \
    apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false

COPY apps /apps.list
RUN sed -i 's@.*REDIS_HOST+x.*@if [ -n "\${REDIS_HOST+x}" ] \&\& [ "\${NO_PHP_REDIS_CONF:-false}" != "true" ]; then@' /entrypoint.sh \
    && NEXTCLOUD_UPDATE=1 /entrypoint.sh "true" \
    && sed -i 's/rsync /echo "Skipped RSYNC" # rsync /g' /entrypoint.sh \
    && for a in $(cat /apps.list); do curl -fSsL $a | tar xvzf - -C apps; done \
    && rm /apps.list \
    && chown -R 33:33 /var/www/html

ENTRYPOINT ["/entrypoint.sh"]
CMD ["apache2-foreground"]