#!/bin/sh

indent(){
    if [ $1 -gt 0 ]; then
	    for i in $(seq 1 $1); do echo -n " "; done
    fi
}

write_config () {
    indent=$1
    shift
    echo "$(indent $indent)" $@ >> /config.php
}

create_config () { 
    echo "Create Config From Env and write it to"
    write_config 0 "<?phg \$CONFIG = array("
    if [ -n "$SQLITE_DATABASE" ];then
        write_config 2 "'dbtype' => 'sqlite',"
        write_config 2 "'dbname' => '$SQLITE_DATABASE',"
    elif [ -n "$MYSQL_DATABASE" ] && [ -n "$MYSQL_USER" ] && [ -n "$MYSQL_PASSWORD" ] && [ -n "$MYSQL_HOST" ] ; then
        write_config 2 "'dbtype' => 'mysql',"
        write_config 2 "'dbname' => '$MYSQL_DATABASE',"
        write_config 2 "'dbuser' => '$MYSQL_USER',"
        write_config 2 "'dbpassword' => '$MYSQL_PASSWORD',"
        write_config 2 "'dbhost' => '$MYSQL_HOST',"
    elif [ -n "$POSTGRES_DB" ] && [ -n "$POSTGRES_USER" ] && [ -n "$POSTGRES_PASSWORD" ] && [ -n "$POSTGRES_HOST" ] ; then
        write_config 2 "'dbtype' => 'pgsql',"
        write_config 2 "'dbname' => '$POSTGRES_DB',"
        write_config 2 "'dbuser' => '$POSTGRES_USER',"
        write_config 2 "'dbpassword' => '$POSTGRES_PASSWORD',"
        write_config 2 "'dbhost' => '$POSTGRES_HOST',"
    fi
    if [ -n "$REDIS_HOST" ]; then
        write_config 2 "'redis' => array("
        write_config 4 "'host' => '$REDIS_HOST',"
        write_config 4 "'port' => $REDIS_HOST_PORT,"
        write_config 4 "'password' => '$REDIS_HOST_PASSWORD',"
        write_config 2 "),"
    fi
    if [ -n "$NEXTCLOUD_TRUSTED_DOMAINS" ]; then
        write_config 2 "'overwrite.cli.url' => 'https://$NEXTCLOUD_TRUSTED_DOMAINS',"
    fi
    # redis, db_prefix, nextcloud_url
    write_config 0 ");"
}

if [ ! -e "/config.php" ] || [ -w "/config.php" ]; then
    create_config
fi

/bin/$(arch -a)/notify_push /config.php