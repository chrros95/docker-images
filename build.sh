#!/bin/bash -eux
BUILD_CONTEXT=$1
VERSION=$2
IMAGE_NAME=$3
IMAGE_TAG=$4
shift;shift;shift;shift

BUILD_DIR="/work"
mkdir -p ${BUILD_DIR}
cp -r ${BUILD_CONTEXT}/* ${BUILD_DIR}
sed -i "s/%%VERSION%%/${VERSION}/g" ${BUILD_DIR}/Dockerfile

/kaniko/executor \
    --cache=true \
    --cache-dir=/cache/kaniko \
    --context ${BUILD_DIR} \
    --dockerfile ${BUILD_DIR}/Dockerfile \
    --destination "${IMAGE_NAME}:${IMAGE_TAG}"

rm -rf $BUILD_DIR